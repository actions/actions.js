var actions = require('actions');

var filmweb = {};

// Makes a http request to http://www.filmweb.pl/search?q={{title argument here}}
filmweb.search = actions.construct([
  actions.construct('util.push', {$push: {title: 'query.q'}}),
  actions.construct('http.request', {url: 'http://www.filmweb.pl/search'}),
]);

// Makes http request to filmweb movie
// And xtracts movie datails from html page
filmweb.extract = actions.construct([
  actions.construct('http.request', {hostname: 'filmweb.pl'}),
  actions.construct('html.extract', {
    selectors: {
      original_title: 'div.filmTitle h2',
      description: 'p.text',
      title: 'div.filmTitle h1',
      image: {
        $extract: 'href',
        $path: 'div.posterLightbox a'
      }
    }
  })
]);

// Makes a search on filmweb
// Get's first result
// And extracts it's details
filmweb.find = actions.construct([
  'filmweb.search',
  actions.construct('html.extract', {selectors: {pathname: {$extract: 'href', $path: 'ul.resultsList li:first-child h3 a'}}}),
  'filmweb.extract'
]);

actions.register('filmweb', filmweb);

var title = process.argv.slice(2).join(' ');
console.log('Looking for "' + title + '"');

actions.run('filmweb.find', {title: title}).then(function(res) {
  console.log('Poster:', res.image);
  console.log('Title:', res.title);
  console.log('Description:', res.description);
});
