var actions = require('actions');

var google = {};

google.search = actions.construct([
  actions.construct('util.push', {
    $push: {
      phrase: 'query.q'
    }
  }),
  actions.construct('http.get', {
    url: 'https://www.google.com/search'
  })
]);

google.results = actions.construct('html.extract', {
  selectors: {
    results: {
      $path: 'li.g',
      $extract: [{
        title: 'h3',
        href: {
          $path: 'a',
          $extract: 'href'
        }
      }]
    }
  }
});

google.find = actions.construct([
  'google.search',
  'google.results'
]);

module.exports = google;
