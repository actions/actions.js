var actions = require('actions');

var opengraphContent = function (name) {
  return {
    $path: 'meta[property="og:'+name+'"]',
    $extract: 'content'
  };
};

var metaContent = function (name) {
  return {
    $path: 'meta[name="'+name+'"]',
    $extract: 'content'
  };
};

var htmlMeta = actions.construct([
  actions.construct('http.request'),
  actions.construct('html.extract', {
    selectors: {
      title: 'title',
      links: {
      	icon: {
      		$path: 'link[rel$="icon"]',
      		$extract: 'href'
      	},
      	canonical: {
      		$path: 'link[rel="canonical"]',
      		$extract: 'href'
      	},
      },
      meta: {
      	image: { $path: 'meta[itemprop="image"]', $extract: 'content' },
      	author: metaContent('author'),
      	description: metaContent('description'),
      	keywords: metaContent('keywords')
      },
      opengraph: {
        url: opengraphContent('url'),
        title: opengraphContent('title'),
        image: opengraphContent('image'),
        description: opengraphContent('description'),
        type: opengraphContent('type'),
        video: {
          url: opengraphContent('video'),
          type: opengraphContent('video:type'),
          height: opengraphContent('video:height'),
          width: opengraphContent('video:width')
        }
      }
    }
  })
]);

actions.register('html.meta', htmlMeta);
