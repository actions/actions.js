var actions = require('actions');

actions.core.add('noop', function() {
  return;
});

var noop = actions.construct('noop');

var todo = 100000;

console.log('running ' + todo + ' noop ' + ' actions');

var start = new Date();
var fnc = function() {
return;
};

for (var i = todo - 1; i >= 0; i--) {
  fnc();
}

var end = new Date();
var rawTime = end - start;
console.log('Raw:', rawTime, 'ms');

var start = new Date();
for (var i = todo - 1; i >= 0; i--) {
  actions.run(noop, false, null, true);
}

var end = new Date();
var actionsTime = end - start;

console.log('Actions:', actionsTime, 'ms');

var rawPerSecond = Math.floor(1000 / (rawTime / todo));
console.log('rawPerSecond: ', rawPerSecond);

var actPerSecond = Math.floor(1000 / (actionsTime / todo));
console.log('actPerSecond: ', (actPerSecond));

console.log('really bad!');

/*
Raw: 5 ms
Actions: 248 ms
rawPerSecond:  20000000
actPerSecond:  403000
really bad!
*/

console.log('100k calls overhead', ((actionsTime - rawTime) / todo) * 100000, 'ms');
