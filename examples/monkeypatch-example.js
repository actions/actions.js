var actions = require('actions');

// html.meta action
require('./html.meta');

// makes actions callable `eq. html.meta(...)`
actions.patch();

var testUrl = "http://www.youtube.com/watch?v=-xR2xlPkNa4";

// If in Browser :)
if (typeof window !== 'undefined') {
  testUrl = "/youtube.html";
}

html.meta({url: testUrl}).then(function (result) {
  console.log(result);
});


/*
Result for url http://www.youtube.com/watch?v=-xR2xlPkNa4 =>

{ title: '2Pac - Ballad of a Dead Soulja PL Napisy serwis Poznaj Tupaca - YouTube',
  links:
   { icon: 'http://s.ytimg.com/yts/img/favicon-vfldLzJxy.ico',
     canonical: 'http://www.youtube.com/watch?v=-xR2xlPkNa4' },
  meta:
   { image: '',
     author: '',
     description: 'Tekst tłumaczenia został udostępniony za zgodą autora i pochodzi ze strony www.poznaj-tupaca.prv.pl Ballada Zmarłego Żołnierza (Ballad of a Dead Soulja) albu...',
     keywords: '2Pac, Ballad, of, Dead, Soulja, PL, Napisy, serwis, Poznaj, Tupaca' },
  opengraph:
   { url: 'http://www.youtube.com/watch?v=-xR2xlPkNa4',
     title: '2Pac - Ballad of a Dead Soulja PL Napisy serwis Poznaj Tupaca',
     image: 'http://i1.ytimg.com/vi/-xR2xlPkNa4/hqdefault.jpg',
     description: 'Tekst tłumaczenia został udostępniony za zgodą autora i pochodzi ze strony www.poznaj-tupaca.prv.pl Ballada Zmarłego Żołnierza (Ballad of a Dead Soulja) albu...',
     type: 'video',
     video:
      { url: 'http://www.youtube.com/v/-xR2xlPkNa4?autohide=1&version=3',
        type: 'application/x-shockwave-flash',
        height: '360',
        width: '480' } } }
*/
