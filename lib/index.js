var Core = require('actions-core/core');
var Promise = require('bluebird');
var utils = require('./utils');
var core = require('actions-core');

/**
 * @namespace
 */
var actions = module.exports;

// Actions core functions
actions.core = core.core;

// Actions registry (empty)
actions.registry = core.actions;

/**
 * Adds next action of action
 *
 * @static
 * @memberOf actions
 * @category Building
 * @param {Object} action Action to set next on.
 * @param {Object} next Next action.
 * @returns {Object} action Last action
 *
 */
actions.setLast = function(action, next) {
  if (typeof action != 'object') {
    throw new Error('To set next action has to be an object');
  }

  if (action.next) {
    return actions.setLast(action.next, next);
  }

  action.next = next;

  return next;
};

// Construct action from array
var constructActions = function(actionsList) {
  var next = null;
  var first = null;
  actionsList.map(function(action, i) {
    // This part is used to handle format like
    //   var fooBarAction = actions.construct([
    //     ['foo.bar', {test: 'param'}],
    //     ['foo.bar2', {not: true}]
    //   ]);
    if (action.constructor === Array) {
      var name = action[0],
        args = action[1],
        extra = action[2];
      action = actions.construct(name, args, extra);
    } else if (action.constructor == String) {
      action = actions.construct(action);
    } else {
      action = utils.clone(action);
    }

    // Action is first
    if (first === null) {
      first = next = action;
    } else {
      next = actions.setLast(next, action);
    }
  });

  return first;
};

/**
 * Constructs an action
 *
 * @static
 * @memberOf actions
 * @category Building
 * @param {Object|Array|string} action Action or action name.
 * @param {Object} [args] Action arguments.
 * @returns {Object}
 * @example
 *
 * var google = actions.construct('http.get', {url: 'http://www.google.pl/'});
 * // => {name: 'http.get', {url: 'http://www.google.pl/'}}
 * 
 * var googleTitle = actions.construct([
 *  google,
 *  actions.construct('html.extract', {selectors: {title: 'head > title'}})
 * ]);
 * // => {name: 'http.get', {url: 'http://www.google.pl/'}, next: {name: 'html.extract', {selectors: {title: 'head > title'}}}}
 *
 */
actions.construct = function(action, args, extra) {
  // we have call like
  // actions.construct({...})
  // actions.construct('my.defined')
  // actions.construct([{...}, {...}])
  if (arguments.length == 1) {
    if (action.constructor === Array) {
      return constructActions(action);
    }
    if (action.constructor === String) {
      return {
        name: action
      };
    }
    if (action.constructor === Object) {
      return action;
    }
    throw new Error('actions.construct got not expected argument: ' + action);
  }

  // we have a call like
  //   var google = actions.construct('http.get', { url: 'someUrl' })
  // or
  //   var google = actions.construct(somefancyparser.crawl, {url: 'http://google.pl/'})
  //   var response = google()
  var result;
  if (action.constructor == Object) {
    result = utils.clone(action);
  } else if (action.constructor == String) {
    result = {name: action};
  } else if (action.constructor === Array) {
    result = constructActions(action);
  }

  if (result.args && args) {
    utils.extend(result.args, args);
  } else if (args) {
    result.args = args;
  }

  // extra can be a object containing fields like $use, $push etc.
  if (extra && extra.constructor == Object) {
    return utils.extend(extra, result);
  }

  return result;
};

actions.func = function (action) {
  return function (args) {
    return actions.run(action, args);
  };
};
 
/**
 * Gets an action from registry
 *
 * @static
 * @memberOf actions
 * @category Registry
 * @param {Object} [registry] Actions registry (default `actions.registry`).
 * @param {string} action Action name (dot separated).
 * @returns {Object}
 * @example
 *
 * actions.get("my.func")
 * // => {name: "my.func", next: {...}}
 *
 * actions.get(myRegistry, "my.func")
 * // => {name: "my.func", next: {...}}
 *
 */
actions.get = function(registry, name) {
  // this `if` handles this pattern
  // function (name)
  if (name === undefined || name === null) {
    name = registry;
    registry = actions.registry;
  }
  var action = utils.get(actions.registry, name);
  if (action) {
    return utils.clone(action);
  }
};

/**
 * Merges `alias` arguments into `action` arguments and set's `alias` as next action.
 *
 * @static
 * @memberOf actions
 * @category Registry
 * @param {Object} action Action to merge on.
 * @param {Object} alias Action to merge (only `args` and `next` are used)
 * @returns {Object}
 *
 */
actions.inherit = function(action, alias) {
  // If action has some arguments, merge it
  if (action.args) {
    utils.extend(action.args, alias.args);
  } else {
    action.args = alias.args;
  }

  // add alias.next as next action
  if (alias.next) {
    actions.setLast(action, alias.next);
  }

  return action;
};

// Run's core function by `action.name` using `action.args`
//  then run's again on `action.next` (if any)
//  otherwise calls back with result (if callback)
var runCore = function(action, callback) {
  actions.core.run(action, action.args, function(result) {
    // Run next action if any
    if (action.next) {
      if (action.next.args) {
        utils.extend(action.next.args, result);
      } else {
        action.next.args = result;
      }
      return runCore(action.next, callback);
    } else {
      if (callback) {
        return callback(result);
      }
    }
  });
};

var coreToFunc = function (object, functions) {
  for (var name in functions) {
    var fnc = functions[name];
    if (fnc.constructor === Function) {
      object[name] = actions.core.func(fnc);
    } else if (fnc.constructor === Object) {
      if (!object[name]) {
        object[name] = {};
      }
      coreToFunc(object[name], functions[name], fnc);
    }
  }
};

var actionsToFunctions = function (object, list) {
  for (var name in list) {
    var action = list[name];
    if (action.name || action.args) {
      object[name] = actions.func(action);
    } else {
      if (!object[name]) {
        object[name] = {};
      }
      actionsToFunctions(object[name], list[name], action);
    }
  }
};

/**
 * Monkey patches core and actions into object.
 *
 * @static
 * @param {Object} [this] Context to patch into
 * @memberOf actions
 * @category Running
 * @example
 *
 * // in node and in browser
 * actions.patch();
 *
 */
actions.patch = function (context) {
  if (typeof context === 'undefined') {
    if (typeof window !== 'undefined') {
      context = window;
    } else if (typeof global !== 'undefined') {
      context = global;
    } else {
      throw new Error('Cannot patch without context variable set');
    }
  }
  coreToFunc(context, actions.core);
  actionsToFunctions(context, actions.registry);
};

/**
 * Runs an action
 *
 * @static
 * @memberOf actions
 * @category Running
 * @param {Object|Array|string} action Action or action name.
 * @param {Function|Boolean} [callback] Callback (function or `false`).
 * @param {Object} [args] Action arguments.
 * @returns {Promise} (if callback was not set)
 * @example
 *
 * actions.run('http.get', {url: 'http://www.google.pl/'}).then(function(response) {
 *   console.log(response.status);
 * });
 * // => '200 OK'
 */
actions.run = function(action, callback, args, keep) {
  // If callback is object - it's arguments
  if (callback && callback.constructor === Object) {
    // callback is args
    keep = args;
    args = callback;
    callback = null;
  }

  // Merge args into action
  if (!keep && args && args.constructor === Object) {
    action = actions.construct(action, args);
  }

  var fnc = function(resolve) {
    if (keep) {
      return runCore(action, resolve);
    } else {
      return runCore(actions.resolve(action), resolve);
    }
  };

  // If callback is false run without it
  if (callback === false) {
    return fnc();
  }

  // If provided a callback run without a promise
  if (callback) {
    return fnc(callback);
  }

  var resolver = Promise.pending();

  // Resolve promise
  fnc(function (result) {
    resolver.resolve(result);
  });

  return resolver.promise;
};

// TODO
actions.remote = function() {
  // gets actions from `url` api
  // and appends results to `actions.registry`
};

/**
 * Registers an action to registry.
 *
 * @static
 * @memberOf actions
 * @category Registry
 * @param {Object} [registry] Actions registry (default `actions.registry`).
 * @param {string} name Action name (dot separated).
 * @param {Object} action Action.
 * @returns {Object}
 * @example
 *
 * actions.register(myLib, 'func', myFunc)
 * // equivalent  => myLib['func'] = myFunc
 * actions.register('my', myLib)
 * // equivalent => actions.register['my'] = myLib
 *
 */
actions.register = function(registry, name, action) {
  // this if handles this pattern
  // function (name, action, registry=actions.registry)
  if (action === undefined || action === null) {
    action = name;
    name = registry;
    registry = actions.registry;
  }

  var exists = actions.get(name);
  var copy = utils.clone(action);

  // If exists merge into a new object
  if (exists) {
    var newCopy = {};
    utils.extend(newCopy, exists);
    utils.extend(newCopy, copy);
    copy = newCopy;
  }

  utils.set(registry, name, copy);
};

/**
 * Resolves action aliases into raw core function call set
 *
 * @static
 * @memberOf actions
 * @category Registry
 * @param {string|Object} action Action or its name
 * @returns {Object}
 * @example
 *
 */
actions.resolve = function (action) {
  if (typeof action === 'string') {
    return actions.resolve({name: action});
  }

  // Look in core
  if (actions.core.has(action.name)) {
    // If action has next action -> resolve it
    if (action.next) {
      action.next = actions.resolve(action.next);
    }

    return action;
  }

  // Get actions by it's name
  var result = actions.get(action.name);
  if (!result) {
    throw new Error("Action #{action.name} doesn't exist");
  }

  // Merge alias args
  if (result.args && action.args) {
    utils.extend(result.args, action.args);
  } else if (action.args) {
    result.args = utils.clone(action.args);
  }

  // Copy next action
  if (action.next) {
    actions.setLast(result, action.next);
  }

  return actions.resolve(result);
};
