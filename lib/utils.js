var lodash = require('lodash');
var extend = require('util')._extend;
var tea = require('tea-properties');

var utils = module.exports = {
  clone: lodash.cloneDeep,
  extend: extend,
  each: lodash.each,
  set: tea.set
};

utils.get = function(object, name) {
  if (object[name]) {
    return object[name];
  }
  
  // this function has to be replaced to improve performance
  return tea.get(object, name);
};
