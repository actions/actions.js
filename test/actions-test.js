var prettyjson = require('prettyjson');
var actions = require('../');
var buster = require('buster');
var assert = buster.referee.assert;

// Expose describe and it functions globally
buster.spec.expose();

// for slow connections
buster.testRunner.timeout = 15000;

var googleTitle = JSON.parse('{"name":"http.get","args":{"url":"http://www.google.com/"},"next":{"name":"html.extract","args":{"selectors":{"title":"head > title"}}}}');

var exampleAction = function() {
  return actions.construct([
    'http.get',
    'html.extract'
  ]);
};

describe('actions.setLast', function() {
  var action = actions.construct([
    'http.get',
    'html.extract'
  ]);

  it('add next action', function() {
    var next = actions.construct('http.post');
    actions.setLast(action, next);
    assert.equals(action.next.next, next);
  });
});

describe('actions.construct', function() {
  var action = exampleAction();
  var result = {
    name: 'http.get',
    next: {
      name: 'html.extract'
    }
  };

  it('constructs a valid action', function() {
    assert.equals(action, result);
  });
});

describe('actions.register', function() {
  var action = exampleAction();
  var lib = {
    test2: actions.construct('http.post'),
    test: action
  };

  it('registers actions in actions.registry', function() {
    actions.register('lib', lib);
    assert.equals(actions.registry.lib, lib);
  });
});

describe('actions.get', function() {
  var action = exampleAction();
  var lib = {
    test: action
  };

  it('returns action by dot separated name', function() {
    actions.register('lib', lib);
    var test = actions.get('lib.test');
    assert.equals(test, action);
  });
});

describe('actions.resolve', function() {
  actions.register('lib', {
    test2: actions.construct('http.post'),
    test: actions.construct([
      exampleAction(),
      'lib.test2'
    ])
  });

  actions.register('my', {
    one: actions.construct('lib.test', {
      my: true
    }),
    second: actions.construct([
      'my.one',
      'http.get'
    ])
  });

  var resolved = {
    name: 'http.request',
    args: {
      method: 'GET',
      my: true
    },
    next: {
      name: 'html.extract',
      next: {
        name: 'http.request',
        args: {
          method: 'POST'
        },
        next: {
          name: 'http.request',
          args: {
            method: 'GET'
          }
        }
      }
    }
  };

  var fromFunc = actions.resolve('my.second');

  it('resolves a function to raw call set', function() {
    assert.equals(fromFunc, resolved);
  });
});

describe('actions.run', function() {
  it('reads "Google" title from google.com', function(done) {
    actions.run(googleTitle).then(function(response) {
      assert.equals(response.title, "Google");
      // => "Google"
      done();
    }).catch(function (err) {
      done(err);
    });
  });
  
  it('reads "Twitter" title from twitter.com', function(done) {
    actions.run(googleTitle, {url: "http://twitter.com"}).then(function(response) {
      assert.equals(response.title, "Twitter");
      // => "Twitter"
      done();
    }).catch(function (err) {
      done(err);
    });
  });
});

describe('actions.patch', function() {

  actions.register('google.title', googleTitle);
  actions.patch();

  it('should make google.title callable', function() {

    assert.equals(typeof google, 'object');
    assert.equals(typeof google.title, 'function');
  });
  
  it('google.title() should be possible now', function(done) {
    google.title().then(function(response) {
      assert.equals(response.title, "Google");
      // => "Google"
      done();
    }).catch(function (err) {
      done(err);
    });
  });
});
