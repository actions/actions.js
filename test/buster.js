var config = module.exports;

config["actions tests"] = {
    environment: "node",  // or "node"
    rootPath: "../",
    sources: [
        "lib/index.js",      // Paths are relative to config file
        "lib/**/*.js"        // Glob patterns supported
    ],
    tests: [
        "test/*-test.js"
    ]
};
